//SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import './KnightFamily.sol';

contract KnightBattle is KnightFamily {

    uint victoryProbability = 70;
    
    event BattleResults(bool _result, uint _knightWin, uint _knightLose);
    event checkresoult(int one, int two);

    function attack(uint _attackknightID, uint _defenseKnightID) external  onlyOwnerOfKnight(_attackknightID)  {
        Knight storage myKnight = listOfKnight[_attackknightID];
        KnightAttack storage myInfoKnight    = propertyAttackKnight[_attackknightID];
        KnightAttack storage enemyInfoKnight = propertyAttackKnight[_defenseKnightID];
        require(_isReady(myKnight),"Knight can't fight yet myKnight");
        require(msg.sender != ownerOf(_defenseKnightID), "You can't attack your knight");
        int powerMyKnight    = int(int32(myInfoKnight.healthPoint) + int32(myInfoKnight.defense) + int32(myInfoKnight.excitementPoint)  -  int32(enemyInfoKnight.damage) * 10);
        int powerEnemyKnight = int(int32(enemyInfoKnight.healthPoint) + int32(enemyInfoKnight.defense) - int32(myInfoKnight.damage) * 10) ;

        if(powerMyKnight >= powerEnemyKnight) {
            unchecked {
                myInfoKnight.winCount++;
                enemyInfoKnight.lostCount++;
            }
            rewardForBattle(_attackknightID);
            if(myKnight.level < 100) {
                _levelUp(_attackknightID);
            } else {
                _mintElixir(1, 3);
            }
            emit BattleResults(true, _attackknightID, _defenseKnightID);
        } else {
            unchecked {
                enemyInfoKnight.winCount++;
                myInfoKnight.lostCount++;
            }
            _triggerCoolDown(myKnight);
            emit BattleResults(false, _defenseKnightID, _attackknightID);
        }
        emit checkresoult(powerMyKnight, powerEnemyKnight);
    }
}