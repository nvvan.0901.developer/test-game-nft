//SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import './KnightUpgrade.sol';

contract KnightQuery is KnightUpgrade {
    
    function _getKnightbyOwner(Knight[] memory _listKnight, address _owner) 
        internal view
        returns(Knight[] memory) 
    {
        Knight[] memory listKnight = new Knight[](_listKnight.length);
        for(uint i = 0; i < _listKnight.length; i++){
            if(listOfKnight[ _listKnight[i].id].owner == _owner) {
                listKnight[i] = listOfKnight[ _listKnight[i].id];
            }
        }
        return listKnight;
    }  

    function getKnightbyOwner(address _owner) 
        external  view
        returns(Knight[] memory)
    {
        return  _getKnightbyOwner(knightOfOwner[_owner], _owner);
    }

    function _getDetailKnight(Knight[] memory _listKnight, address _owner) 
        internal view 
        returns(Knight[] memory , KnightAttack[] memory) 
    {
        Knight[] memory listKnight = new Knight[](_listKnight.length);
        KnightAttack[] memory listAttack = new KnightAttack[](_listKnight.length);
        for(uint i = 0; i < _listKnight.length; i++){
            if(listOfKnight[ _listKnight[i].id].owner == _owner) {
                listKnight[i] = listOfKnight[ _listKnight[i].id];
                listAttack[i] = propertyAttackKnight[_listKnight[i].id];
            }
        }

        return (listKnight, listAttack);
    }  

    function getKnigtDetail() 
        external view  
        returns(Knight[] memory , KnightAttack[] memory) 
    {
        return  _getDetailKnight(knightOfOwner[_msgSender()], _msgSender());
    }

    function _getElixirbyOwner(Elixir[] memory _listElixir) 
        internal view
        returns(Elixir[] memory) 
    {
        uint listLenght = _listElixir.length;
        Elixir[] memory elixirs = new Elixir[](listLenght);
        for(uint i = 0; i < listLenght; i++){
            elixirs[i] = listElixir[_listElixir[i].id];
        }
        return elixirs;
    }  

    function getElixirbyOwner()
        external view
        returns(Elixir[] memory)
    {
        return _getElixirbyOwner(elixirOfOwner[_msgSender()]);
    }
}