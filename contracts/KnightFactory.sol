//SPDX-License-Identifier: MIT
pragma solidity >= 0.5.0 <0.9.0;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "./KnightERC1155.sol";
contract KnightFactory is KnightERC1155 {
    using Strings for uint256;
    using Counters for Counters.Counter;

    Counters.Counter internal _tokenIdCounter;

    string baseURIKnight = "ipfs://QmeAPecjTgMb5YBofM3HjT1gSCz7WvtgZRERxUFLJiE4zh/";
    string baseImage = "https://opensea.mypinata.cloud/ipfs/QmWvM6vkqjZnDcHoZFxnC8D68ZQcoNVnVnCArYE6ciS8dp/";
    uint decimel = 18;
    uint tokenSTI = 10 ** decimel;
    uint coolDownTime = 1 minutes;
    uint randNonce = 0;
    uint coolDownSex =  7 hours;
    uint amountOfpage = 7;
    uint32 timeGiftOne = 480 seconds; // 30 * 480 = 4 hours
    uint32 timeGiftTwo = 540 seconds; // 40 * 540 = 6 hours
    uint32 timeGiftThree = 600 seconds; // 30 * 600 = 5 hours
    
    event NewKnight(Knight _newKnight, KnightAttack _newAttackKnight);
    
    struct Knight {
        address owner;
        string  name;
        string  image;
        uint    id;
        uint    indexImg;
        uint32  level;
        uint32  attackTime;
        uint32  sexTime;
        uint32  defaultAttack;
        uint32  defaultSex;
        uint8   star;
    }

    struct KnightAttack {
        uint32 healthPoint;
        uint32 damage;
        uint32 defense;
        uint32 excitementPoint;
        uint16 winCount;
        uint16 lostCount;
    }
    
    Knight[] internal listOfKnight;
    KnightAttack[] internal propertyAttackKnight;
    mapping(address => bool) private initPlay;
    mapping(uint256 => address) private ownerKnight;
    mapping(address => Knight[]) public knightOfOwner; //internal 
    function _createKnight(Knight memory _newKnight, KnightAttack memory _newAttackKnight, address _owner) 
        internal 
    {
        uint256 knightID = _tokenIdCounter.current();
        uint randImage = randTokenUri(amountOfpage, knightID);
        _newKnight.attackTime = uint32(block.timestamp + coolDownTime);
        _newKnight.sexTime = uint32(block.timestamp + coolDownSex);
        _newKnight.id = knightID;
        _newKnight.indexImg = randImage;
        _newKnight.image = string(abi.encodePacked(baseImage, Strings.toString(randImage), "/" , "1.png"));
        listOfKnight.push(_newKnight);
        knightOfOwner[_owner].push(_newKnight);
        propertyAttackKnight.push(_newAttackKnight);
        _mint(_owner, KNIGHT, 1, "");
        // _setTokenURI(knightID, string(abi.encodePacked(baseURIKnight, Strings.toString(randImage), "/" , "1.json")));
        ownerKnight[knightID] = _owner;
        _tokenIdCounter.increment();
        emit NewKnight(_newKnight, _newAttackKnight);
    }

    function randMod(uint _modulus) 
        internal returns(uint) 
    {
        randNonce++;
        return uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, randNonce))) % _modulus;
    }

    function initialPlay(string memory _name) 
        public 
    {
        address account = _msgSender();
        require(initPlay[account] == false, "Create a knight only for the first time");
        Knight memory knight =  Knight(msg.sender , _name, "", 0, 0, 1, 0, 0, 0, 0, 1);
        KnightAttack memory knightAtt = KnightAttack(1000, 100, 100, 0, 0, 0);
        _createKnight(knight, knightAtt, account);
        _mint(account, STICOIN, 100 * tokenSTI, " ");
        initPlay[account] = true; 
    }


    function getInitPlay(address _owner)
        external view returns(bool) 
    {
        return initPlay[_owner];
    }

    function ownerOf(uint _knightID) 
        public view returns (address) 
    {
        address owner = ownerKnight[_knightID];
        require(owner != address(0), "ERC721: invalid token ID");
        return owner;
    }

    function setAmountOfPage(uint _newNumber) 
        public onlyOwner 
    {
        amountOfpage = _newNumber;
    }
    
    function getAmountOfPage() 
        external view returns (uint)
    {
        return amountOfpage;
    }

    function randTokenUri(uint _modulus, uint knightID) 
        internal  returns(uint) 
    {
        randNonce++;
        return uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, knightID, randNonce))) % _modulus;
    }

    function setBaseUri(string memory _newURI) 
        external onlyOwner 
    {
        baseURIKnight = _newURI;
    }
    function setBaseImage(string memory _newURI) 
        external onlyOwner 
    {
        baseImage = _newURI;
    }
}