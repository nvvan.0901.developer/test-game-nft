//SPDX-License-Identifier: MIT
pragma solidity >= 0.5.0 <0.9.0;

import "./KnightFactory.sol";
contract KnightItems is KnightFactory {
    using Strings for uint256;
    using Counters for Counters.Counter;

    Counters.Counter internal _elixirIdCounter;
    uint priceLv1 = 3 * tokenSTI;  // 3 STIcoin
    uint priceLv2 = 5 * tokenSTI;
    uint priceLv3 = 8 * tokenSTI;

    enum ElixirLevel{ Nonlevel , LevelOne , LevelTwo, LevelThree}

    struct Elixir {
        ElixirLevel level;
        uint id;
        address owner;
    }

    Elixir[] internal listElixir;

    mapping(uint => address) internal balancesElixir;
    mapping(address => Elixir[]) internal elixirOfOwner;
    event BuyElixir(address account, uint level, uint[] elixirID, uint amount);

    function buyElixir(uint _level, uint _amount, uint _deposit) 
        external  
    {
        require(_level <= uint(ElixirLevel.LevelThree) , "KinghtItems: Invalid level elixir");

        if(_level == uint(ElixirLevel.LevelOne)) {
            require(_deposit / _amount == priceLv1, "insufficient supply of STI Coin");
        } else if(_level == uint(ElixirLevel.LevelTwo)) {
            require(_deposit / _amount == priceLv2, "insufficient supply of STI Coin");
        } else if(_level == uint(ElixirLevel.LevelThree)) {
            require(_deposit / _amount == priceLv3, "insufficient supply of STI Coin");
        } else {
            require(false, "KinghtItems: Invalid level elixir");
        }
        burn(msg.sender, STICOIN, _deposit);
        uint[] memory listElixirID = _mintElixir(_amount, uint(_level));
        emit BuyElixir(msg.sender, uint(_level), listElixirID, _amount);
    }

    function _mintElixir(uint _amount, uint _level) internal returns (uint[]  memory ) {
        uint[] memory  listID = new uint[](_amount);
        for (uint256 i = 0; i < _amount; i++) {
            uint elixirID  = _elixirIdCounter.current(); 
            listElixir.push(Elixir(ElixirLevel(_level), elixirID, msg.sender));
            listID[i] = elixirID;
            elixirOfOwner[msg.sender].push(Elixir(ElixirLevel(_level), elixirID, msg.sender));
            balancesElixir[elixirID] = msg.sender;
            _elixirIdCounter.increment();
        }
        _mint(msg.sender, ELIXIR, _amount, "");
        return listID;
    }

    function ownerOfElixir(uint _elixirID) 
        public view returns(address) 
    {
        address owner = balancesElixir[_elixirID];
        require(owner != address(0), "ERC721: invalid elixir ID");
        return owner;
    }

}