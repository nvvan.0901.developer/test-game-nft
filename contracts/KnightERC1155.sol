// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Burnable.sol";

contract KnightERC1155 is ERC1155, Ownable, ERC1155Burnable {
    uint256 public constant ELIXIR = 0;
    uint256 public constant STICOIN = 1;
    uint256 public constant KNIGHT = 2;
    constructor()
        ERC1155("ipfs://QmeAPecjTgMb5YBofM3HjT1gSCz7WvtgZRERxUFLJiE4zh/{id}.json")
    {
        _mint(address(0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2), STICOIN, 1000 * 10 ** 18, "");
    }

    function setURI(string memory newuri) public onlyOwner {
        _setURI(newuri);
    }

    function mint(address account, uint256 id, uint256 amount, bytes memory data)
        public
        onlyOwner
    {
        _mint(account, id, amount, data);
    }

    function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data)
        public
        onlyOwner
    {
        _mintBatch(to, ids, amounts, data);
    }
}