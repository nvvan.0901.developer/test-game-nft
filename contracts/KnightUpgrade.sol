//SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import './KnightBattle.sol';

contract KnightUpgrade is KnightBattle {
    
    modifier onlyOwnerOfElixir(address _owner , uint _elixirID) {
        require(ownerOfElixir(_elixirID) == msg.sender, "invalid owner of Elixir");
        _;
    }

    function useElixir(uint _knightID, uint _elixirID) 
        external 
        onlyOwnerOfKnight(_knightID) 
        onlyOwnerOfElixir(msg.sender,_elixirID)
    {
        Elixir memory myElixir = listElixir[_elixirID];
        KnightAttack storage propertyAttack = propertyAttackKnight[_knightID];
        if(myElixir.level == ElixirLevel.LevelThree ) {
            propertyAttack.healthPoint += 100;
            propertyAttack.damage      += 60;
            propertyAttack.defense     += 20;
        } else if(myElixir.level == ElixirLevel.LevelTwo ) {
            propertyAttack.healthPoint += 50;
            propertyAttack.damage      += 20;
            propertyAttack.defense     += 10;
        } else if(myElixir.level == ElixirLevel.LevelOne) {
            propertyAttack.healthPoint += 20;
            propertyAttack.damage      += 10;
            propertyAttack.defense     += 5;
        }
        burn(msg.sender, ELIXIR, 1);
        uint lenght = elixirOfOwner[msg.sender].length;
        for (uint256 index = 0; index < lenght; index++) {
            if(elixirOfOwner[msg.sender][index].id == _elixirID)
            {
                delete elixirOfOwner[msg.sender][index];
            }
        }
        delete listElixir[_elixirID];
        delete balancesElixir[_elixirID];
    }
   
}