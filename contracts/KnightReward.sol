//SPDX-License-Identifier: MIT
pragma solidity >= 0.5.0 < 0.9.0;

import './KnightItems.sol';

contract KnightReward is KnightItems {

    mapping(address => bool) private firstRechargeGame;

    modifier onlyOwnerOfKnight(uint _knightID) {
        require(ownerOf(_knightID) == msg.sender,"Invalid owner of the Knight");
        _;
    }
    event TriggerCoolDown(uint knightID, uint timeOut);
    event Lucky(uint number);

    function rewardForBattle(uint _knightID) internal onlyOwnerOfKnight(_knightID) {
        Knight storage myknight = listOfKnight[_knightID];
        Knight memory knight =  Knight(msg.sender,"NoName", "", 0, 0, 1, 0, 0, 0, 0, 1);
        KnightAttack memory knightAtt = KnightAttack(1000,100, 100, 0, 0, 0);
        uint luckyNumber = randMod(1000);
        if(luckyNumber <= 700) {
            _mintElixir(3, 1);
            _mint(msg.sender, STICOIN, 3 * tokenSTI, "");
        } else if(luckyNumber <= 950 && luckyNumber > 700) {
            _mintElixir(2, 2);
            _mint(msg.sender, STICOIN, 4 * tokenSTI, "");
        }else if(luckyNumber <= 999 && luckyNumber > 950) {
            _mintElixir(1, 3);
            _mint(msg.sender, STICOIN, 6 * tokenSTI, "");
        }
        _createKnight(knight, knightAtt, _msgSender());
        _triggerCoolDown(myknight);
        emit Lucky(luckyNumber);
    } 

    function _triggerCoolDown(Knight storage _knight) internal {
        _knight.attackTime = uint32(block.timestamp + coolDownTime - _knight.defaultAttack);
        emit TriggerCoolDown(_knight.id, _knight.attackTime);
    }

    function _isReady(Knight storage _knight) internal view returns(bool) {
        return _knight.attackTime <= block.timestamp;
    }

    function firstRecharge() external payable {
        address account = _msgSender();
        require(firstRechargeGame[account] == false, "KnightReward: you are not eligible");
        require(msg.value == 0.005 ether, "KnightReward: Insufficient supply of ether");
        _mint(account, STICOIN, 10 * tokenSTI, "");
        _mintElixir(5, 1);
        _mintElixir(3, 2);
        _mintElixir(2, 3);
        firstRechargeGame[account] = true; 
        // phần thưởng tương đương với 1 STIcoin = 0.148 dola;
    }
}