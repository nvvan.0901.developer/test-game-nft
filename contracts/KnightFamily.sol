//SPDX-License-Identifier: MIT
pragma solidity >= 0.5.0 < 0.9.0;

import './KnightHelper.sol';

contract KnightFamily is KnightHelper {
    using Strings for uint256;
    using Counters for Counters.Counter;

    Counters.Counter internal _marriageIdCounter;
    uint weddingExpenses = 2 * tokenSTI;
    uint minAmountgift = 6 * tokenSTI;

    mapping(uint => mapping(uint => uint)) public registerMarry;
    mapping(uint => bool) public KnightEngaged;

    event RequestMarry (uint IDknightRequest, uint IDknightResponse, address from, address to, uint amountGift, uint marriageID);
    event ApprovalMarry(uint IDknightRequest, uint IDknightResponse, bool resoult);
    event TriggerTired (uint knightID, uint timeOut);

    struct Marriage {
        uint32    idMarriage;
        uint32    idKinghtRequest;
        uint32    idKinghtResponse;
        uint32    timeWedding;
        address   ownerKnightRequest;
        address   ownerKnightResponse;
        bool      status;
        uint      giftAmount;
    }

    Marriage[] public listMarriage;

    modifier acceptedMarry(uint _IDknightRequest, uint _IDknightResponse) {
        uint  marriageID = registerMarry[_IDknightRequest][_IDknightResponse];
        Marriage storage myMarriage = listMarriage[marriageID];
        require(myMarriage.status == true, "Married before running this function");
        _;
    }

    modifier isBetrothed(uint _IDknightRequest, uint _IDknightResponse) {
        require(KnightEngaged[_IDknightRequest] && KnightEngaged[_IDknightResponse], "Not betrothed");
        _;
    }

    function requestMarry(uint32 _IDknightRequest, uint32 _IDknightResponse, uint _diposit) 
        external 
        aboveLevel(20, _IDknightRequest)
        aboveLevel(18, _IDknightResponse)
        onlyOwnerOfKnight(_IDknightRequest)
    {
        require(_diposit >= minAmountgift, "Gift quantity less than 6 STICoin");
        require(!KnightEngaged[_IDknightRequest] && !KnightEngaged[_IDknightResponse], "Knight is engaged!");
        address ownerRequest  = ownerOf(_IDknightRequest);
        address ownerResponse = ownerOf(_IDknightResponse);
        uint256 marriageID = _marriageIdCounter.current();
        registerMarry[_IDknightRequest][_IDknightResponse] = marriageID;
        listMarriage.push(Marriage(uint32(marriageID), _IDknightRequest, _IDknightResponse, 0, ownerRequest, ownerResponse, false, _diposit));
        KnightEngaged[_IDknightRequest]  = true;
        KnightEngaged[_IDknightResponse] = true;
        _marriageIdCounter.increment();
        emit RequestMarry(_IDknightRequest, _IDknightResponse, ownerRequest, ownerResponse,  _diposit, marriageID);
    }

    function approveMarry(uint _IDknightRequest, uint _IDknightResponse, bool _resoult) 
        external 
        onlyOwnerOfKnight(_IDknightResponse) 
        isBetrothed(_IDknightRequest, _IDknightResponse)
    {
        address ownerRequest  = ownerOf(_IDknightRequest);
        address ownerResponse = ownerOf(_IDknightResponse);
        uint  marriageID = registerMarry[_IDknightRequest][_IDknightResponse];
        Marriage storage myMarriage = listMarriage[marriageID];
        require(myMarriage.status == false, "This marriage is consensual");
        if(_resoult) {
            uint moneyWedding = myMarriage.giftAmount;
            myMarriage.status = true;
            myMarriage.timeWedding = uint32(block.timestamp) + 7 days;
            uint gift = moneyWedding - weddingExpenses;
            _safeTransferFrom(ownerRequest, ownerResponse, STICOIN, gift, "");
            _burn(ownerRequest, STICOIN, weddingExpenses);
        } else {
            _destroyMarry(_IDknightRequest, _IDknightResponse, false);
        }
       
        emit ApprovalMarry(_IDknightRequest, _IDknightResponse, _resoult);
    }

    function destroyMarry(uint _IDknightRequest, uint _IDknightResponse) 
        external  
    {
        address ownerRequest  = ownerOf(_IDknightRequest);
        address ownerResponse = ownerOf(_IDknightResponse);
        require(ownerRequest == msg.sender || ownerResponse == msg.sender, "Invalid owner of knight");
        if(ownerRequest == msg.sender) {
            _destroyMarry(_IDknightRequest, _IDknightResponse, false);
        } else if(ownerResponse == msg.sender)
        {
            _destroyMarry(_IDknightRequest, _IDknightResponse, true);
        }
    }

    function _destroyMarry(uint _IDknightRequest, uint _IDknightResponse, bool _checkTime) 
        internal  
    {
        if(_checkTime) {
            uint  marriageID = registerMarry[_IDknightRequest][_IDknightResponse];
            Marriage storage myMarriage = listMarriage[marriageID];
            require(myMarriage.timeWedding < block.timestamp,"Cancel after 7 days of marriage");
            delete listMarriage [registerMarry[_IDknightRequest][_IDknightResponse]];
            delete registerMarry[_IDknightRequest][_IDknightResponse];    
            delete KnightEngaged[_IDknightRequest];
            delete KnightEngaged[_IDknightResponse];
        } else {
            delete listMarriage [registerMarry[_IDknightRequest][_IDknightResponse]];
            delete registerMarry[_IDknightRequest][_IDknightResponse];    
            delete KnightEngaged[_IDknightRequest];
            delete KnightEngaged[_IDknightResponse];
        }
    }

    

    function interCourseKnight(uint _knightIdOne, uint _knightIdTwo) 
        external  
        acceptedMarry(_knightIdOne, _knightIdTwo) 
    {
        Knight storage myKnight = listOfKnight[_knightIdOne];
        Knight storage loverKnight = listOfKnight[_knightIdTwo];
        require(_isReadySex(myKnight) && _isReadySex(loverKnight) ,"Knight can't fight yet");
        _reproductionKnight(myKnight, loverKnight, _knightIdOne, _knightIdTwo);
    }

    function _isReadySex(Knight storage _knight) 
        internal view 
        returns(bool) 
    {
        return _knight.sexTime <= block.timestamp;
    }

    function _reproductionKnight(Knight storage _knightFather, Knight storage _knightMother, uint _IDknightRequest, uint _IDknightResponse) 
        internal 
    {
        uint randWhoParent = randMod(100);
        uint32 avgLevel = uint32(_knightFather.level +  _knightMother.level) / 4;
        uint32 defaultAttackTime ; 
        uint32 defaultSexTime ; 
        uint32 healthPoint;
        uint32 damage;
        uint32 defense;
        if(avgLevel <= 30) {
            defaultAttackTime = avgLevel * timeGiftOne;
            defaultSexTime    = avgLevel * timeGiftThree;
            healthPoint       = avgLevel * 20;
            damage            = avgLevel * 5;
            defense           = avgLevel * 2;
        }
        Knight memory knightBaby = Knight(msg.sender,"BabyKnight", "", 0, 0, avgLevel, uint32(coolDownTime), uint32(coolDownSex), defaultAttackTime, defaultSexTime,  1);
        KnightAttack memory knightAttBaby = KnightAttack(healthPoint, damage, defense, 0, 0, 0);
        if(randWhoParent <= 50) 
        {
            _createKnight(knightBaby, knightAttBaby, ownerOf(_IDknightRequest));
        } else  {
            _createKnight(knightBaby, knightAttBaby, ownerOf(_IDknightResponse));
        }

        _triggerTired(_knightFather);
        _triggerTired(_knightMother);
    }
    function _triggerTired(Knight storage _knight) 
        internal 
    {
        _knight.sexTime = uint32(block.timestamp + coolDownSex - _knight.defaultSex);
        emit  TriggerTired(_knight.id, _knight.sexTime);
    }

    function setMinAmountgift(uint _newPrice) 
        external 
        onlyOwner
    {
        minAmountgift = _newPrice;
    }

    function setWeddingExpenses(uint _newPrice) 
        external 
        onlyOwner 
    {
        weddingExpenses = _newPrice;
    }

}