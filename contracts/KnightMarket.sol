//SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import './KnightQuery.sol';

contract KnightMarket is KnightQuery {
    
    event ChangeOwnerKnight(uint _knightID, address _newOwner);
    event Result(bool result);
    function safeTransferFrom(address from, address to, uint256 id, uint256 amount, bytes memory data)
        public  virtual override(ERC1155) 
    {
        // data = 0x000
        KnightMarket.changeOwnerKnight(bytesToUint(data), to);
        super.safeTransferFrom(from, to, id, amount, data);
        
    }
    
    function bytesToUint(bytes memory b) internal pure returns (uint256){
            uint256 number;
            for(uint i=0;i<b.length;i++){
                number = number + uint(uint8(b[i]))*(2**(8*(b.length-(i+1))));
            }
        return number;
    }

    function changeOwnerKnight(uint _knightID, address _newOwner) internal {
        address ownerOld = listOfKnight[_knightID].owner;
        Knight[]  storage listKnightOld = knightOfOwner[ownerOld];
        uint lenght = listKnightOld.length;
        for (uint256 index = 0; index < lenght; index++) {
            if(listKnightOld[index].id == _knightID) {
                delete knightOfOwner[ownerOld][index];
            }
        }
        listOfKnight[_knightID].owner = _newOwner;
        knightOfOwner[_newOwner].push(listOfKnight[_knightID]);
        emit ChangeOwnerKnight(_knightID, _newOwner);
       
    }
}