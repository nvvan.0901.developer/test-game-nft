//SPDX-License-Identifier: MIT
pragma solidity >= 0.5.0 <0.9.0;  

import './KnightReward.sol';

contract KnightHelper is KnightReward {

    uint payTolevelUp = 1 * tokenSTI;
    
    modifier aboveLevel(uint _level, uint _knightID) {
        require(listOfKnight[_knightID].level >= _level, string(abi.encodePacked("Upgrade the knight to level ", Strings.toString(_level))));
        _;
    }
    
    event ChangeName(uint _knightID, string _newName);
    event LevelUp(uint _knightID, uint _newlevel);
    event ExcitementPointUp(uint _knightID, uint _newPoint);
    
    receive() external payable {}

    fallback() external payable {}

    function changeName(uint _knightID, string calldata _newName) 
        external 
        aboveLevel(2, _knightID) 
        onlyOwnerOfKnight(_knightID) 
    {
        listOfKnight[_knightID].name = _newName;
        emit ChangeName(_knightID, _newName);
    }

    function levelUp(uint _knightID, uint _deposit) 
        external  
    {
        require(_deposit == payTolevelUp, "insufficient supply of STI Coin");
        _levelUp(_knightID);
        burn(msg.sender, STICOIN, _deposit);
    }

    function _levelUp(uint _knightID) internal  {
        Knight storage myKnight = listOfKnight[_knightID];
        KnightAttack storage propertyAttack =  propertyAttackKnight[_knightID];
        require(myKnight.level < 100, "Knight is max level");
        if(  myKnight.level < 30) {
            myKnight.level++;
            myKnight.defaultAttack     += timeGiftOne;
            myKnight.defaultSex        += timeGiftThree;
            propertyAttack.healthPoint += 20;
            propertyAttack.damage      += 5;
            propertyAttack.defense     += 2;
        } else if( myKnight.level < 70) {
            myKnight.level++;
            myKnight.defaultAttack     += timeGiftTwo;
            myKnight.defaultSex        += timeGiftTwo;
            propertyAttack.healthPoint += 30;
            propertyAttack.damage      += 10;
            propertyAttack.defense     += 5;
        } else {
            myKnight.level++;
            myKnight.defaultAttack     += timeGiftThree;
            myKnight.defaultSex        += timeGiftOne;
            propertyAttack.healthPoint += 50;
            propertyAttack.damage      += 25;
            propertyAttack.defense     += 15;
        }
        if(myKnight.level == 31) {
            myKnight.star = 2;
            myKnight.image = string(abi.encodePacked(baseImage, Strings.toString( myKnight.indexImg), "/" , "2.png"));
            // _setTokenURI(myKnight.id, string(abi.encodePacked(baseURIKnight, Strings.toString( myKnight.indexImg), "/" , "2.json"))); // Lên Sao sẽ được thay thế image theo cấp sao
        } else if( myKnight.level == 71) {
            myKnight.star = 3;
            myKnight.image = string(abi.encodePacked(baseImage, Strings.toString( myKnight.indexImg), "/" , "3.png"));
            // _setTokenURI(myKnight.id, string(abi.encodePacked(baseURIKnight, Strings.toString( myKnight.indexImg), "/" , "3.json"))); 
        }

        emit LevelUp(_knightID, myKnight.level);
    }

    function setLevel30(uint _knightID) external {
        Knight storage myKnight = listOfKnight[_knightID];
        KnightAttack storage propertyAttack =  propertyAttackKnight[_knightID];
            myKnight.level  = 30 ;
            myKnight.defaultAttack     = 14400;
            myKnight.defaultSex        = 18000;
            propertyAttack.healthPoint = 1600;
            propertyAttack.damage      = 250;
            propertyAttack.defense     = 160;
    }

    function knightGoMassage(uint _knightID) 
        external payable 
    {
        require(msg.value == payTolevelUp);
        KnightAttack storage myInfoKnight =  propertyAttackKnight[_knightID];
        require(myInfoKnight.excitementPoint < 200,"Knight has maxed out excitement points");
        myInfoKnight.excitementPoint += 2;
        emit ExcitementPointUp(_knightID,  myInfoKnight.excitementPoint);
    }

    function withdraw() 
        external 
        onlyOwner
    {
        payable(owner()).transfer(address(this).balance);
    }

    function setPaytoLevelUp(uint _newPrice) 
        external 
        onlyOwner 
    {
        payTolevelUp = _newPrice;
    }

    
}